-- CREAO LA BASE DE DATOS EMPRESA
create database empresa;


-- SELECCIONO ESTA BBDD
use empresa;


-- CREO LA TABLA DEPARTAMENTOS
create table departamentos(
    codigo int(6) primary key not null,
    nombre varchar(30) not null,
    presupuesto int(9) not null
);


-- CREO LA TABLA EMPLEADOS
create table empleados (
    DNI varchar(9) not null primary key,
    nombre varchar(30) not null,
    apellidos varchar(80) not null,
    dpto int(6) not null
);


-- INSERTO DIFERENTES REGISTROS EN LA TABLA DEPARTAMENTOS
insert into departamentos VALUES
    (000001, 'Ventas', 100000000),
    (000002, 'Marketing', 200000000),
    (000003, 'Creatividad', 100000000),
    (000004, 'Administración', 300000000)


-- INSERTO DIFERENTES REGISTROS EN LA TABLA EMPLEADOS
insert into empleados values 
    ('70023425D', 'Guillermo', 'Montero Martín', 000004),
    ('70076523G', 'Irene', 'Sánchez Baranda', 000004),
    ('70078339L', 'Aroa', 'Iglesias Junior', 000002),
    ('70023991R', 'Jorge', 'Sacristán Ortega', 000001),
    ('70029873H', 'Roberto', 'Fernández Larios', 000003),
    ('70099012F', 'David', 'Rodríguez Maroto', 000002),
    ('70087182F', 'Alberto', 'Esteban Sánchez', 000001),
    ('70002889S', 'María', 'Escarcha Martín', 000003),
    ('70055662X', 'Ana', 'Martínez San Segundo', 000003),
    ('70076192F', 'Ylenia', 'Menéndez Castro', 000002),
    ('70009126G', 'Marta', 'Varela Gómez', 000001),
    ('70081662D', 'Mario', 'Quesada Cervantes', 000001),
    ('70073785D', 'Jorge', 'Montero Martín', 000003);


-- MUESTRO LOS PAELLIDOS DE TODOS LOS EMPLEADOS
mysql> select apellidos from empleados;
+-----------------------+
| apellidos             |
+-----------------------+
| Escarcha Martín       |
| Varela Gómez          |
| Montero Martín        |
| Sacristán Ortega      |
| Fernández Larios      |
| Martínez San Segundo  |
| Montero Martín        |
| Menéndez Castro       |
| Sánchez Baranda       |
| Iglesias Junior       |
| Quesada Cervantes     |
| Esteban Sánchez       |
| Rodríguez Maroto      |
+-----------------------+


-- MUESTRO LOS APELLIDOS DE TODOS LOS EMPLEADOS SIN REPETIR APELLIDOS
mysql> select distinct apellidos from empleados;
+-----------------------+
| apellidos             |
+-----------------------+
| Escarcha Martín       |
| Varela Gómez          |
| Montero Martín        |
| Sacristán Ortega      |
| Fernández Larios      |
| Martínez San Segundo  |
| Menéndez Castro       |
| Sánchez Baranda       |
| Iglesias Junior       |
| Quesada Cervantes     |
| Esteban Sánchez       |
| Rodríguez Maroto      |
+-----------------------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS CUYOS APELLIDOS SON 'MONTERO MARTÍN'
mysql> select * from empleados where apellidos = "Montero Martín";
+-----------+-----------+-----------------+------+
| DNI       | nombre    | apellidos       | dpto |
+-----------+-----------+-----------------+------+
| 70023425D | Guillermo | Montero Martín  |    4 |
| 70073785D | Jorge     | Montero Martín  |    3 |
+-----------+-----------+-----------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS CUYOS APELLIDOS CONTIENEN 'MONTERO'
mysql> select * from empleados where apellidos like "%Montero%";
+-----------+-----------+-----------------+------+
| DNI       | nombre    | apellidos       | dpto |
+-----------+-----------+-----------------+------+
| 70023425D | Guillermo | Montero Martín  |    4 |
| 70073785D | Jorge     | Montero Martín  |    3 |
+-----------+-----------+-----------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS CUYOS APELLIDOS CONTENGAN 'MONTERO' O/Y 'SÁNCHEZ'
mysql> select * from empleados where apellidos like "%Montero%" or apellidos like "%Sánchez%";
+-----------+-----------+------------------+------+
| DNI       | nombre    | apellidos        | dpto |
+-----------+-----------+------------------+------+
| 70023425D | Guillermo | Montero Martín   |    4 |
| 70073785D | Jorge     | Montero Martín   |    3 |
| 70076523G | Irene     | Sánchez Baranda  |    4 |
| 70087182F | Alberto   | Esteban Sánchez  |    1 |
+-----------+-----------+------------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS QUE TRABAJAN EN EL DEPARTAMENTO 3
mysql> select * from empleados where dpto = 3;
+-----------+---------+-----------------------+------+
| DNI       | nombre  | apellidos             | dpto |
+-----------+---------+-----------------------+------+
| 70002889S | María   | Escarcha Martín       |    3 |
| 70029873H | Roberto | Fernández Larios      |    3 |
| 70055662X | Ana     | Martínez San Segundo  |    3 |
| 70073785D | Jorge   | Montero Martín        |    3 |
+-----------+---------+-----------------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS QUE TRABAJAN EN LOS DEPARTAMENTOS 2 Y 3
mysql> select * from empleados where dpto = 3 or dpto = 2;
+-----------+---------+-----------------------+------+
| DNI       | nombre  | apellidos             | dpto |
+-----------+---------+-----------------------+------+
| 70002889S | María   | Escarcha Martín       |    3 |
| 70029873H | Roberto | Fernández Larios      |    3 |
| 70055662X | Ana     | Martínez San Segundo  |    3 |
| 70073785D | Jorge   | Montero Martín        |    3 |
| 70076192F | Ylenia  | Menéndez Castro       |    2 |
| 70078339L | Aroa    | Iglesias Junior       |    2 |
| 70099012F | David   | Rodríguez Maroto      |    2 |
+-----------+---------+-----------------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS CUYOS APELLIDOS EMPIEZAN POR M
mysql> select * from empleados where apellidos like "M%";
+-----------+-----------+-----------------------+------+
| DNI       | nombre    | apellidos             | dpto |
+-----------+-----------+-----------------------+------+
| 70023425D | Guillermo | Montero Martín        |    4 |
| 70055662X | Ana       | Martínez San Segundo  |    3 |
| 70073785D | Jorge     | Montero Martín        |    3 |
| 70076192F | Ylenia    | Menéndez Castro       |    2 |
+-----------+-----------+-----------------------+------+


-- MUESTRO TODOS LOS DATOS DE LOS DEPARTAMENTOS REGISTRADOS EN LA TABLA DEPARTAMENTOS
mysql> select * from departamentos;
+--------+-----------------+-------------+
| codigo | nombre          | presupuesto |
+--------+-----------------+-------------+
|      1 | Ventas          |   100000000 |
|      2 | Marketing       |   200000000 |
|      3 | Creatividad     |   100000000 |
|      4 | Administración  |   300000000 |
+--------+-----------------+-------------+


-- MUESTRO EL PRESUPUESTO TOTAL DE LA EMPRESA/DE TODOS LOS DEPARTAMENTOS JUNTOS
mysql> select sum(presupuesto) from departamentos;
+------------------+
| sum(presupuesto) |
+------------------+
|        700000000 |
+------------------+


-- MUESTRO LA CANTIDAD DE EMPLEADOS EN CADA DEPARTAMENTO
mysql> select count(*) from empleados group by dpto;
+----------+
| count(*) |
+----------+
|        4 |
|        4 |
|        2 |
|        3 |
+----------+


-- MUETRO LA CANTIDAD DE EMPLEADOS Y EL NOMBRE DEL DEPARTAMENTO DE CADA DEPARTAMENTO AGRUPADOS POR DEPARTAMENTOS
mysql> select count(*), d.nombre from empleados e join departamentos d on d.codigo = e.dpto group by d.nombre;
+----------+-----------------+
| count(*) | nombre          |
+----------+-----------------+
|        4 | Creatividad     |
|        4 | Ventas          |
|        2 | Administración  |
|        3 | Marketing       |
+----------+-----------------+


-- MUESTRO TODOS LOS DATOS DE LOS EMPLEADOS Y EL DEPARTAMENTO PARA EL QUE TRABAJAN
mysql> select * from empleados e join departamentos d on d.codigo = e.dpto;
+-----------+-----------+-----------------------+------+--------+-----------------+-------------+
| DNI       | nombre    | apellidos             | dpto | codigo | nombre          | presupuesto |
+-----------+-----------+-----------------------+------+--------+-----------------+-------------+
| 70002889S | María     | Escarcha Martín       |    3 |      3 | Creatividad     |   100000000 |
| 70009126G | Marta     | Varela Gómez          |    1 |      1 | Ventas          |   100000000 |
| 70023425D | Guillermo | Montero Martín        |    4 |      4 | Administración  |   300000000 |
| 70023991R | Jorge     | Sacristán Ortega      |    1 |      1 | Ventas          |   100000000 |
| 70029873H | Roberto   | Fernández Larios      |    3 |      3 | Creatividad     |   100000000 |
| 70055662X | Ana       | Martínez San Segundo  |    3 |      3 | Creatividad     |   100000000 |
| 70073785D | Jorge     | Montero Martín        |    3 |      3 | Creatividad     |   100000000 |
| 70076192F | Ylenia    | Menéndez Castro       |    2 |      2 | Marketing       |   200000000 |
| 70076523G | Irene     | Sánchez Baranda       |    4 |      4 | Administración  |   300000000 |
| 70078339L | Aroa      | Iglesias Junior       |    2 |      2 | Marketing       |   200000000 |
| 70081662D | Mario     | Quesada Cervantes     |    1 |      1 | Ventas          |   100000000 |
| 70087182F | Alberto   | Esteban Sánchez       |    1 |      1 | Ventas          |   100000000 |
| 70099012F | David     | Rodríguez Maroto      |    2 |      2 | Marketing       |   200000000 |
+-----------+-----------+-----------------------+------+--------+-----------------+-------------+


-- MUESTRO EL NOMBRE Y APELLIDOS DE LOS EMPLEADOS ASÍ COMO EL NOMBRE DEL DEPARTAMENTO PARA EL QUE TRABAJAN JUNTO AL PRESUPUESTO DE DICHO DEPARTAMENTO
mysql> select e.nombre, e.apellidos, d.nombre as departamento, d.presupuesto from empleados e join departamentos d on d.codigo = e.dpto;
+-----------+-----------------------+-----------------+-------------+
| nombre    | apellidos             | departamento    | presupuesto |
+-----------+-----------------------+-----------------+-------------+
| María     | Escarcha Martín       | Creatividad     |   100000000 |
| Marta     | Varela Gómez          | Ventas          |   100000000 |
| Guillermo | Montero Martín        | Administración  |   300000000 |
| Jorge     | Sacristán Ortega      | Ventas          |   100000000 |
| Roberto   | Fernández Larios      | Creatividad     |   100000000 |
| Ana       | Martínez San Segundo  | Creatividad     |   100000000 |
| Jorge     | Montero Martín        | Creatividad     |   100000000 |
| Ylenia    | Menéndez Castro       | Marketing       |   200000000 |
| Irene     | Sánchez Baranda       | Administración  |   300000000 |
| Aroa      | Iglesias Junior       | Marketing       |   200000000 |
| Mario     | Quesada Cervantes     | Ventas          |   100000000 |
| Alberto   | Esteban Sánchez       | Ventas          |   100000000 |
| David     | Rodríguez Maroto      | Marketing       |   200000000 |
+-----------+-----------------------+-----------------+-------------+


-- MUESTRO EL DNI, NOMBRE Y APELLIDOS DE LOS EMPLEADOS ASÍ COMO EL NOMBRE DEL DEPARTAMENTO PARA EL QUE TRABAJAN JUNTO AL CÓDIO Y PRESUPUESTO DE DICHO DEPARTAMENTO ORDENADOS POR DEPARTAMENTO
mysql> select DNI, e.nombre, e.apellidos, d.nombre as departamento, d.codigo, presupuesto from empleados e join departamentos d on e.dpto = d.codigo order by d.nombre;
+-----------+-----------+-----------------------+-----------------+--------+-------------+
| DNI       | nombre    | apellidos             | departamento    | codigo | presupuesto |
+-----------+-----------+-----------------------+-----------------+--------+-------------+
| 70023425D | Guillermo | Montero Martín        | Administración  |      4 |   300000000 |
| 70076523G | Irene     | Sánchez Baranda       | Administración  |      4 |   300000000 |
| 70002889S | María     | Escarcha Martín       | Creatividad     |      3 |   100000000 |
| 70029873H | Roberto   | Fernández Larios      | Creatividad     |      3 |   100000000 |
| 70055662X | Ana       | Martínez San Segundo  | Creatividad     |      3 |   100000000 |
| 70073785D | Jorge     | Montero Martín        | Creatividad     |      3 |   100000000 |
| 70076192F | Ylenia    | Menéndez Castro       | Marketing       |      2 |   200000000 |
| 70078339L | Aroa      | Iglesias Junior       | Marketing       |      2 |   200000000 |
| 70099012F | David     | Rodríguez Maroto      | Marketing       |      2 |   200000000 |
| 70009126G | Marta     | Varela Gómez          | Ventas          |      1 |   100000000 |
| 70023991R | Jorge     | Sacristán Ortega      | Ventas          |      1 |   100000000 |
| 70081662D | Mario     | Quesada Cervantes     | Ventas          |      1 |   100000000 |
| 70087182F | Alberto   | Esteban Sánchez       | Ventas          |      1 |   100000000 |
+-----------+-----------+-----------------------+-----------------+--------+-------------+


-- MUESTRO EL NOMBRE Y APELLIDOS DE LOS EMPLEADOS QUE TRABAJAN EN DEPARTAMENTOS CON PRESUPUESTOS SUPERIORES A 200000000
mysql> select e.nombre as Nombre, e.apellidos as Apellidos from empleados e join departamentos d on e.dpto = d.codigo where d.presupuesto > 200000000;
+-----------+------------------+
| Nombre    | Apellidos        |
+-----------+------------------+
| Guillermo | Montero Martín   |
| Irene     | Sánchez Baranda  |
+-----------+------------------+


-- MUESTRO TODOS LOS DATOS DE LOS DEPARTAMENTOS CUYO PRESUPUESTO ES SUPERIOR A LA MEDIA DEL PRESUPUESTO DE LA EMPRESA/DEPARTAMENTOS
mysql> select * from departamentos where presupuesto > (select avg(presupuesto) from departamentos);
+--------+-----------------+-------------+
| codigo | nombre          | presupuesto |
+--------+-----------------+-------------+
|      2 | Marketing       |   200000000 |
|      4 | Administración  |   300000000 |
+--------+-----------------+-------------+


-- MUESTRO EL NOMBRE DE LOS DEPARTAMENTOS QUE TIENEN MÁS DE 2 EMPLEADOS CONTRATADOS
mysql> select d.nombre from departamentos d join empleados e on d.codigo = e.dpto group by d.nombre having count(*) > 2;
+-------------+
| nombre      |
+-------------+
| Creatividad |
| Ventas      |
| Marketing   |
+-------------+


-- AGREGO EL DEPARTAMENTO 'CALIDAD' A LA TABLA DEPARTAMENTOS
mysql> insert into departamentos values (11, "Calidad", 40000);


-- MUESTRO LA TABLA DEPARTAMENTOS ACTUALIZADA
mysql> select * from departamentos;
+--------+-----------------+-------------+
| codigo | nombre          | presupuesto |
+--------+-----------------+-------------+
|      1 | Ventas          |   100000000 |
|      2 | Marketing       |   200000000 |
|      3 | Creatividad     |   100000000 |
|      4 | Administración  |   300000000 |
|     11 | Calidad         |       40000 |
+--------+-----------------+-------------+


-- CONTRATO UNA PERSONA PARA EL DEPARTAMENTO DE CALIDAD
mysql> insert into empleados values ("89267109B", "Esther", "Vázquez Darín", 11);


-- MUESTRO LA TABLA DE EMPLEADOS ACTUALIZADA
mysql> select * from empleados;
+-----------+-----------+-----------------------+------+
| DNI       | nombre    | apellidos             | dpto |
+-----------+-----------+-----------------------+------+
| 70002889S | María     | Escarcha Martín       |    3 |
| 70009126G | Marta     | Varela Gómez          |    1 |
| 70023425D | Guillermo | Montero Martín        |    4 |
| 70023991R | Jorge     | Sacristán Ortega      |    1 |
| 70029873H | Roberto   | Fernández Larios      |    3 |
| 70055662X | Ana       | Martínez San Segundo  |    3 |
| 70073785D | Jorge     | Montero Martín        |    3 |
| 70076192F | Ylenia    | Menéndez Castro       |    2 |
| 70076523G | Irene     | Sánchez Baranda       |    4 |
| 70078339L | Aroa      | Iglesias Junior       |    2 |
| 70081662D | Mario     | Quesada Cervantes     |    1 |
| 70087182F | Alberto   | Esteban Sánchez       |    1 |
| 70099012F | David     | Rodríguez Maroto      |    2 |
| 89267109B | Esther    | Vázquez Darín         |   11 |
+-----------+-----------+-----------------------+------+


-- LES REDUZCO UN 10% EL PRESUPUESTO A CADA DEPARTAMENTO
mysql> update departamentos set presupuesto = presupuesto-(presupuesto*0.1);


-- MUESTRO LA TABLA DEPARTAMENTOS ACTUALIZADA
mysql> select * from departamentos;
+--------+-----------------+-------------+
| codigo | nombre          | presupuesto |
+--------+-----------------+-------------+
|      1 | Ventas          |    90000000 |
|      2 | Marketing       |   180000000 |
|      3 | Creatividad     |    90000000 |
|      4 | Administración  |   270000000 |
|     11 | Calidad         |       36000 |
+--------+-----------------+-------------+


-- CAMBIO A LOS EMPLEADOS DEL DEPARTAMENTO CON CÓDIGO 3 AL DEPARTAMENTO CON CÓDIGO 77
mysql> update empleados set dpto = 77 where dpto = 3;


-- DESPIDO A LOS EMPLEADOS DEL DEPARTAMENTO 2
mysql> delete from empleados where dpto = 2;


--  DESPEDIMOS A LOS EMPLEADOS QUE TRABAJAN EN DEPARTAMENTOS CON PRESUPUESTOS SUPERIORES A 260000000
mysql> delete e from empleados e join departamentos d on e.dpto = d.codigo where d.presupuesto > 260000000;


-- DESPIDO A TODOS LOS EMPLEADOS
mysql> delete from empleados;